![](pics/me.png#profilepic)

I am Jan and a huge fan of falafel sandwiches. 
Otherwise I like computer science, long hikes, spring, open source, games, 
and the internet as a medium for people to connect. 
<!-- I do not like cars, (neoliberal) capitalism, injustice, conspiracy theories and 
bad-faith arguments. -->

My passion for the last years has been computer graphics, especially
real-time graphics for games. In particular: ray tracing, path tracing,
physically based lighting, volumetric and atmospheric rendering, 
data oriented design and GPU debugging.

I am currently employed at Ubisoft Mainz and working 
on [Anno 117](https://www.anno-union.com/announcing-anno-117-pax-romana/)
as a Graphics Programmer. I have some (more or less maintained) open
source projects:

- [Vulkan Introspection Layer]({{< ref "/posts/vil.md" >}}) for live GPU debugging
  with Vulkan ([Github](https://github.com/nyorain/vil))
- [dlg (Github)](https://github.com/nyorain/dlg), a simple C library for
  logging and assertions, allowing for custom handlers.

Feel free to contact me via email, nyorain@gmail.com. Or chat me up
on [mastodon](https://mas.to/@blurple)!

