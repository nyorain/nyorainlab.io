---
title: 'Light Path Guided Culling for Hybrid Real-Time Path Tracing'
date: 2024-07-23
layout: lipac
aliases:
    - /publications/lipac
params:
    authors:
        - Jan Kelling
        - Daniel Ströter
        - Arjan Kuijper
    teaser:
        - img: "/pics/teaser-opt.png"
          title: "Hybrid path tracing with LiPaC"
        - img: "/pics/teaser-boxes.png"
          title: "Acceleration structure visualization"
    downloads:
        - name: "ACM Publication (Open Access)"
          path: "https://doi.org/10.1145/3675387"
          icon: "fa-link"
        - name: "Video Summary (HPG-Fast-Forward at SIGGRAPH 2024)"
          path: "/lipac.mp4"
          icon: "fa-play"
        - name: "HPG slides"
          path: "https://www.highperformancegraphics.org/slides24/LiPaC-HPG24-slides.pdf"
          icon: "fa-file-pdf"
        - name: "Paper Preprint"
          path: "/pdf/lipac-preprint.pdf"
          icon: "fa-file-pdf"
    bibtex: |
        @article{lipac,
            author = {Kelling, Jan and Str\"{o}ter, Daniel and Kuijper, Arjan},
            title = {Light Path Guided Culling for Hybrid Real-Time Path Tracing},
            year = {2024},
            volume = {7},
            number = {3},
            doi = {10.1145/3675387},
            journal = {Proc. ACM Comput. Graph. Interact. Tech.},
            month = {aug},
            articleno = {37},
        }

  

---

Rendering visually convincing images requires realistic lighting.
Path tracing has long been used in offline rendering to produce photorealistic
images. While recent hardware advancements allow ray
tracing methods to be employed in real-time renderers, they come
with a significant performance and memory impact. Real-time path
tracing remains a challenge. We present light path guided culling
(LiPaC), a novel culling algorithm for ray tracing that achieves
almost optimal culling results by considering the number of light
paths encountered by objects. In addition, we describe a hybrid
path tracing pipeline using LiPaC to render large and highly dynamic 
scenes in real-time on the current generation of consumer hardware. 

---

Presented at [HPG 2024](https://www.highperformancegraphics.org/2024/index.html).
{{< youtube Q2_TFY8F-s4>}}
