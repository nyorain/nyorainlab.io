---
title: 'Memory-efficient Real-time Path Tracing for Computer Games'
date: 2024-02-19
type: posts
layout: lipac
aliases:
    - /publications/master-thesis
params:
    authors:
        - Jan Kelling
    teaser:
        - img: "/pics/thesis-pt.png"
          title: "Real-time Path tracing in the Anno engine"
    downloads:
        - name: "Full Master Thesis"
          path: "/pdf/Pathtracing-final-compressed.pdf"
          icon: "fa-file-pdf"
---

Rendering visually convincing and realistic images requires accurate lighting
computation. Path tracing has long been used in offline rendering to achieve
this goal. Recent graphics processing unit (GPU) advancements and new sampling
algorithms enable path tracing in real-time. This thesis investigates whether
real-time path tracing on commodity hardware is feasible for the current
generation of video games. We integrate and evaluate a real-time path tracer
into an existing game engine and provide an in-depth investigation of
performance and memory measurements. Our case study is done with Anno, a state
of the art city building game series that renders large and highly dynamic
user-built scenes. We present an efficient hybrid rendering pipeline able to
compute path traced indirect lighting in real-time within the memory limits of
consumer hardware. To solve observed performance and memory issues, we
contribute two novel path tracing methods:

Light path guided culling tracks the number of light paths passing regions of
the scene. The information is used to only selectively build and maintain the
required parts of the scene representation in graphics memory. This technique
allows for increased visual quality, improved performance, and significantly
reduced memory consumption compared to existing simple heuristics for partial
scene representation. This generic method can be combined with various kinds of
hardware accelerated ray tracing methods.


Stochastic vegetation ray skipping is employed to allow fast ray tracing in
scenes with high amounts of vegetation. We present a method employing hybrid
rendering, relying on rasterization for primary rays and direct lighting where
possible, and a heuristic utilizing ray skipping through entire vegetation
instances to speed up indirect lighting computation. This method provides ray
tracing speedups of factors greater than two for vegetation-heavy scenes and
proves crucial to achieving real-time performance.
