---
title: 'Moin!'
date: 2024-06-12T15:07:27+02:00
---

Hey there! Lately, I have been reading more and more about visions of what the 
internet could be. Learning about projects like [indieweb](https://indieweb.org/),
[ActivityPub](https://en.wikipedia.org/wiki/ActivityPub) (the protocol behind
[Mastodon](https://joinmastodon.org/), [Lemmy](https://join-lemmy.org/)
or [Pixelfed](https://pixelfed.org/)) and seeing individuals like
[Molly White](https://www.mollywhite.net/) pointing out web problems and 
sketching out possible alternatives really got me hyped about this idea
of an internet used for open and independent communication between individuals.
The last years, I have been disappointed by many of the online platforms
I have been using (Reddit/Twitter/Github/Instagram/Youtube, the list goes on).

<!---
- [Reddit closed down third party apps](https://en.wikipedia.org/wiki/2023_Reddit_API_controversy),
  only offering their own proprietary, inferior mobile app instead.
  [Reddit ads are blending in more and more with true content](https://www.theregister.com/2024/03/16/reddit_promoted_posts/),
  [they push viewers to log in](https://www.reddit.com/r/help/comments/135tly1/comment/jim40zg/?context=9), 
  [sell user data](https://www.wired.com/story/reddits-sale-user-data-ai-training-draws-ftc-investigation/)
  and the overall trend screams [enshittification](https://en.wikipedia.org/wiki/Enshittification).
- [Twitter is full of hate and toxicity](https://www.latimes.com/business/technology/story/2023-04-27/hate-speech-twitter-surged-since-elon-musk-takeover),
  among [other problems](https://www.communication-generation.com/the-gradual-enshittification-of-twitter-a-close-look-at-the-decline-of-a-social-media-giant-with-answers/)
- Github sold to Microsoft and has been using copyrighted code to train
  their Copilot AI, which is [problematic in its own way](https://githubcopilotinvestigation.com/)
- Instagram and Youtube are being flooded with advertisments,
  attention grabbing and categories of contents that just do not have
  a positive impact on my life.
-->

Most proprietary services just don't feel right to me anymore.
So many websites take ages to load, are full of shitty advertisments, pop-ups,
tracking and someone decided that autoplaying videos is somehow
acceptable now?! What the hell is up with that, by the way?

Having your own blog and interacting with others using open and decentralized
protocols seems like a stark contrast.
And since this is how the internet (apparently) used to work before the
rise of the big internet platforms, there is already some great tech for this.
I recently _discovered_ [RSS](https://en.wikipedia.org/wiki/RSS) --- don't
blame me, I have hardly seen it being discussed anymore --- and it seems like 
such an elegant solution to the problem of "show me all the things I want to read".
Without anyone in between deciding what you read and potentially abusing
this power.

<!-- add more about social media attention span/toxicity issues? !-->
<!-- How it's great to put your *self* out in the world !-->
<!-- Some of the blogs that inspired me !-->

Every now and then, I stumble over random people's blogs. Often I'm
encountering them from a computer science (often computer graphics)
angle but am then even more interested finding all sorts of random
things on their personal web pages: Sketches, short stories, old poems,
created music, opinions about games or shows I'm passionate about,
beautiful small picture albums, I've even come across a personal website
of someone just gathering their favorite jokes.
This feels like the purest and best spirit of the internet to me:
Allowing people to freely express themselves, be themselves. Just for
others to enjoy and possibly connect.

All these considerations and inspiration from other blogs I like to read
has pushed me to start my own little blog. I added some older
posts I wrote somewhere else or just locally to begin with!

<!-- write about technical stack? -->
