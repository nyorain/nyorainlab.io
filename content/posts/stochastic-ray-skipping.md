---
title: 'Stochastic Ray Skipping'
date: 2024-06-23T22:59:11+02:00
draft: true
---

Abstract:
Real-time ray tracing
Vegetation
alpha testing
anyhit shaders
many invocations
performance.

![](/pics/srs/viz-anyhit.png)

![](/pics/pipeline-crop.svg)

Highly scattered rays for GI
approximative solution enough
Novel technique, stochastic ray skipping
significant performance improvements
Approximating geometry stochastically

![](/pics/ray-1.svg)

<!-- could add related work later, optional for now  -->

Method: from slides/thesis

![](/pics/srs/viz-srs.png)

Eval: perf from thesis
graphical from thesis

Future work:
New heuristic ideas.
Describe how it can be combined with LiPaC.
Evaluate again in comparison to microsurfaces.

